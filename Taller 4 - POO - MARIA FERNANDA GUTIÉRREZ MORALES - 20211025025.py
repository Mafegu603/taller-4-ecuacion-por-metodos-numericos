import math

def discriminante(a,b,c):
    discrim=pow(b,2)-(4*a*c)
    return discrim

def raices(a,b,disc):
    raiz1=(-b+math.sqrt(disc))/(2*a)
    raiz2=(-b-math.sqrt(disc))/(2*a)
    return raiz1,raiz2
   
print ("Cálculo de raíces")
a=float(input("a: "))
b=float(input("b: "))
c=float(input("c: "))
disc=discriminante(a,b,c)
if disc<0:
    print("No hay raíces positivas")
else:
    print("Las raíces son:")
    print (raices(a,b,disc))